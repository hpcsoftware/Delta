\documentclass{report}
\usepackage[english]{babel}
\usepackage{graphicx, color, xcolor, amsmath, amssymb, amsthm}
\usepackage{float, caption, subcaption, algorithmicx, algorithm, algpseudocode}
\usepackage{listings, blindtext, framed, datetime, tabularx, nomencl, listings, xcolor}
\usepackage{hyperref}

\lstset { %
    language=C++,
    backgroundcolor=\color{black!5}, % set backgroundcolor
    basicstyle=\footnotesize,% basic font setting
    escapeinside={<@}{@>}
}

\floatname{algorithm}{Algorithm}

\begin{document}

\begin{titlepage}
	\centering
	\includegraphics[width=0.15\textwidth]{figures/logo}\par\vspace{1cm}

	\vspace{1cm}

	{\huge\bfseries Short Delta Guidebook: A Discrete Element Method Library for Tessellated Geometries\par}
	\vspace{2cm}
	{\Large\itshape Konstantinos Krestenitis\par}
	{\scshape\LARGE Durham University \par}	
	\vspace{2cm}
	{\scshape\Large eCSE ARCHER PROJECT 2018\par}
	\vspace{1.5cm}
	\vfill
% Bottom of the page
	{\large \today\par}
\end{titlepage}

\tableofcontents

\input{00_overview}




\chapter{Quick Start}

\section{Quick Standalone Installation}

The directory that holds the code is named ”delta”. A main function can be placed at the root directory to use the library. In the root directory there is a Makefile to compile the code. The Assimp library is required to support import and export of mesh data types. The library supports over 40 formats but the delta library at its basic form offers support for the vtk file format. 

The Assimp library can be found here: http://www.assimp.org/
The library needs to be compiled before the compilation of the delta routines. According to the library INSTALL documentation the code is compiled using "cmake":

\begin{verbatim}
cmake CMakeLists.txt -G 'Unix Makefiles'
make
\end{verbatim}

Then the main code we needs to include and then link the Assimp library during compilation. To link the library just export two environment variables:

\begin{verbatim}
export ASSIMP_INC=-I/Users/kk/libraries/assimp/include/
export ASSIMP_LIB="-L/Users/kk/libraries/assimp/lib/ -lassimp"
\end{verbatim}

Additional libraries "-lz -lrt -lIrrXML" might be required unless they are installed during the compilation of the Assimp library in which case they can be omitted. 

To compile the main we can use Gnu or Intel by running ”make delta-gcc-release” or ”make delta-icc-release”. To debug we use ”make delta-gcc-asserts” or ”make delta-icc-release”. Both commands will compile all source code and produce an executable with the same name. Currently, the library source code comes with a sample main.cpp that runs a template DEM code skeleton. The template code is used as a guideline to basic functionality.

\chapter{Abstract Phase Namespaces}

The source code that is found in the "delta" folder. All functions are documented in the header files of the source code. The source code is structured as follows within five main namespaces. Detailed information can be found in www.peano-framework.org/delta/ doxygen pages and the source code.

\begin{itemize}

\item Contact (Contact Detection Routines and Collision Handlers)

\item Core (Core Simulation Handlers, Trackers, Metrics, I/O)

\item Dynamics (Physics Operators For Orientation Matrix)

\item Geometry (Geometry Operators and Preprocess Body Manipulators)

\item World (World Entinties Layout, Scenario Creators)

\end{itemize}

\section{Contact}

In this namespace we define all functionality relevant to contact detection between two meshes. We define contact points as a structure to conveniently summarise information per contact point. An additional subnamespace ”contact::detection” defines all contact detection methods for meshes as well as spheres.

\section{Core}

In this namespace we define all the IO functionality of the code. We have read/write namespaces to import and export the geometry. In order to support multiple data formats, Assimp library is used. At its basic functionality the code supports a simplified VTK import and export routine. Moreover the core holds a data structure that in the DEM context can hold all the data in one single SoA data structure. Statistics and time measurements can be implemented here.

\section{Dynamics}

In Dynamics namespace we define the functionality that is relevant to physics computations (inertia, mass, center of mass, volume, energy) and energy. Time integration routines are also included here to update angular velocities and orientation matrices. This namespace is relevant to DEM implementations.

\section{Geometry}

In Geometry namespace we define all functionality that is relevant to geometry. Here we define the triangle and mesh class as well as objects. A mesh is com- posed of triangles and a object holds a mesh. Any geometry can be described within a mesh object, that is position, bounding boxes. Routines of meshes implement common procedures that is common to operate on meshes during the initiation phase of the simulation (moveTo(), scale(), getBoundingBox(), etc.). The object construct builds a object that holds in addition to mesh object physical properties (inertia, mass, velocities, etc.). Lastly the namespace holds subnamespace ”geometry::operators” that hold routines that operate on an unstructured mesh, these are required to setup the scene of the initial geom- etry. The ”geometry::hardcoded” holds hardcoded geometries (hopper, blender, granulates, cube).

\section{World}

The world namespace defines the ”world”. That is all particles that are involved in the simulation. In this namespace we include routines to create assembles of particles. Layout subnamespace routines define function that align a set di- mensional array of particles at a position. The layout routine can be used to stack a group of 10x10x10 particles above a hopper structure. The configu- ration subnamespace is used to preconfigure geometry into place and scenario subnamespace define hardcoded simulation scenarios using the imported or pre- defined geometries.

\chapter{Integration Use Cases}

There are a few ways to use the library. Individual functions can be called irrespectively of the DEM workflow as required by the user application. Alternatively, a user can follow the workflow of DEM without looking into the hierachy of the DEM source code. Lastly, different phases of the DEM workflow can be selected or opt out on demand with pragmas and comments either manually or using the flags provided.   

\section{General DEM Template}

A DEM code can be simply implemented by creating a main.cpp file. In the main function we need to wrap in a for loop the following DEM phases:


\subsection{Atomic Operants}
All DEM phases are derived from the basic namespaces that the source code is based upon. Therefore each namespace (except the core) represents the algorithmic phases. More specifically there are only four phases out of which three are repeated per time step.

The DEM algorithm starts off by the creation of the geometric domain. In this phase, the user can choose to either import a geometry or create it manually by hard-coding each triangle. Then to run the simulation, it is required to call a contact detection routine in order to define contact points. The contact points are enventually used to derive a force and to update the position of the geometry utilised per time step.

\begin{itemize}

\item World Creation (The geometry is created only once.)

\item Contact Detection (Performed if collisions are enabled.)

\item Force Derivation (Forces are derived from contacts.)

\item Position Update (Particles are translated at every time step.)

\end{itemize}

{\bf World Creation.} The library under world::scenarios namespace holds predefined scenarios that can be called and run. For example, the "two particle collision" scenario is invoked by passing the epsilon parameter and mesh density. The function call returns a list of objects in which case the "two particle collision" scenario returns two objects. The list of objects are then passed to the simulation.

{\bf Contact Detection.} All contact detection methods are implemented in a atomic functions that are selected according to the contact model used. Contact detection is performed between between two particles and two primitive geometric elements. All collisions are stored locally in a pair-wise map. 

{\bf Forces.} The contact points collision map is then used to define forces at the contact point. These forces are then transfered to the bodies as velocities.  

{\bf Position Update.} The position of the particles is changed by delta x using the velocities that are derived from the forces applied. The position update is performed at every step of the simulation.


\subsection{WorkFlow Constructs}
Alternatively the user can use the constructs to construct and run the DEM code inside predefined wrappers. Following a object oriented approach the whole world creation is defined as an object that is passed to an engine object. Both world and engine constructs define the previously mentioned DEM phases. To execute the DEM code one has to define a DEM delta engine and call iterate() to execute one simulation step.

The following code snippet is the minimum code required to load and run a DEM simulation using the wrapper engine construct:

\begin{lstlisting}
   #include <delta/core/delta.h>
   #include <delta/core/Engine.h>
   #include <delta/world/World.h>
   #include <delta/core/io/HumanInterface.h>
   
   int main(int argc, const char* argv[])
   {
     delta::core::io::HumanInterface human(argc, argv);
     
     auto world = delta::world::World(
     human.getScenario(), 
     false, 
     human.getCollisionScheme(), 
     human.getMeshDensity());
   
     delta::core::Engine engine(world, human.getEngineMeta());
   
     for(int i=0; i<human.getSteps(); i++)
     {
       engine.iterate();
     }
   
     return 0;
   }

\end{lstlisting}

\clearpage

\section{ExaHyPE Template}
We test the delta code on the ExaHyPE project. It is possible to use the contact detection system to map the geometry that lives in delta onto the ExaHyPE grid. As Delta is already supports over 40 mesh data formats it is convinient to use the world creation in ExaHyPE. In addition an independent Delta contact dynamics simulation can run as standalone DEM instance from within ExaHyPE.

To compile the delta library on a ExaHyPE based project one has to append the compiler flags as follows:
\begin{lstlisting}
COMPLILER_CFLAGS+=-DiREAL=double -DbyteAlignment=64 -qopenmp $(ASSIMP_INC)
COMPLILER_LFLAGS+=-fopenmp $(ASSIMP_LIB)
\end{lstlisting}

Where ASSIMP\_INC and ASSIMP\_LIB are the enviroment paths to the assimp library if the assimp library is used.


\subsection{Create a Rigid Body World}
The world creation is only performed once. Any files used by delta is loaded during the initialisation phase of ExaHyPE. The following portion of code is taken from EulerFiniteVolume demostrator code and demostrates the loading of a wind turbine in STL format:

\begin{lstlisting}
#include "MyEulerSolver.h"
#include "MyEulerSolver_Variables.h"
#include "Logo.h"

#include <vector>
#include <array>

#ifdef DeltaLIB
#include "delta/core/io/read.h"
#include "delta/contact/detection/point.h"
#include "delta/core/data/ParticleRecord.h"
#endif

tarch::logging::Log EulerFV::MyEulerSolver::_log
( "EulerFV::MyEulerSolver" );
#ifdef DeltaLIB
delta::core::Engine EulerFV::MyEulerSolver::_deltaEngine;
iREAL EulerFV::MyEulerSolver::_epsilon;
#endif
void EulerFV::MyEulerSolver::init(
const std::vector<std::string>& cmdlineargs,
const exahype::parser::ParserView& constants) {
#ifdef DeltaLIB
    _epsilon = 0.038;

    std::vector<delta::world::structure::Object> particles;
    delta::core::data::Meta::EngineMeta engineMeta;
    engineMeta.plotScheme = delta::core::data::Meta::Plot::Never;
    engineMeta.overlapPreCheck = false;
    engineMeta.modelScheme = 
    delta::core::data::Meta::CollisionModel::none;
    engineMeta.dt = 0.11;
    engineMeta.gravity = false;
    std::array<iREAL, 6> boundary = 
    {0.0, 0.0, 0.0, 1.0, 1.0, 1.0};

    std::array<iREAL, 3> centre  = {0.5, 0.5, 0.5};
    std::array<iREAL, 3> linear  = {0.0, 0.0, 0.0};
    std::array<iREAL, 3> angular = {-2.5, 0.0, 0.0};

    #if 1==2
    delta::geometry::mesh::Mesh *mesh = 
    delta::core::io::readPartGeometry("input/turbine.stl");

    mesh->scaleXYZ(0.5);//either scale model or set domain boundary
    delta::world::structure::Object object(
      "turbine", 0, mesh, centre, 
      delta::geometry::material::MaterialType::WOOD, 
      false, false, true, _epsilon, linear, angular);
    engineMeta.maxPrescribedRefinement = 0.005;

    #else
    delta::geometry::mesh::Mesh *mesh = 
    delta::core::io::readPartGeometry("input/KaikouraUlrichetal.stl");
    mesh->scaleXYZ(0.000005); //either scale model or set domain boundary
    delta::world::structure::Object object(
      "plate", 0, mesh, centre, 
      delta::geometry::material::MaterialType::WOOD, 
      false, false, true, _epsilon, linear, angular);
    engineMeta.maxPrescribedRefinement = 0.01;
    #endif

    particles.push_back(object);
    _deltaEngine = 
    delta::core::Engine(particles, boundary, engineMeta);
#endif
}
\end{lstlisting}

\subsection{Voxellisation and Distance Maps}

\begin{figure}[htb]
  \begin{center}
    \includegraphics[width=0.49\textwidth]{figures/gone.png}
    \includegraphics[width=0.49\textwidth]{figures/blade_voxelisation_0.png}
  \end{center}
  \caption{
    A complex geometry is used as a reference to create voxels.
  }
  \label{figure:voxel}
\end{figure}

During each ExaHyPE traversal adjustSolution is called to solve unknowns. We have defined four additional Q variables in the specification file of the EulerFV project. The objective of this example is to mark ExaHyPE cells using the real physical geometry as a reference. The result of the following snippet is the voxellisation/pixelisation of the geometry:

\begin{lstlisting}
 void EulerFV::MyEulerSolver::adjustSolution(
 const double* const x,
 const double t,
 const double dt, 
 double* Q) 
 {
   // Dimensions             = 3
   // Number of variables    = 9 + #parameters
 
   Variables vars(Q);
 
   for(int ii=0; ii<_deltaEngine.getParticleRecords().size() &&
    _deltaEngine.getState().getCurrentStepIteration() > 0; ii++)
   {
       auto newContactPoints =
        delta::contact::detection::pointToGeometry(
        x[0], x[1], x[2], 0, _epsilon, 
        xCoordinates, yCoordinates, zCoordinates, 
        numberOfTriangles, 1, 0.0);
 
       iREAL distance = 1E99;
       int index = 0;
       for(int i=0; i<newContactPoints.size(); i++)
       {
         iREAL contactDistance = 
         newContactPoints[i].getDistance();
         if(distance > contactDistance)
         {
           distance = contactDistance;
           index = i;
         }
       }
 
       if(newContactPoints.size() > 0)
       {
         //contact found (i.e. distance closer than epsilon)
         vars.d(0) = newContactPoints[index].Q[1];
         vars.d(1) = newContactPoints[index].Q[2];
         vars.d(2) = newContactPoints[index].Q[3];
         vars.d(3) = distance;
 
         if(newContactPoints[index].penetration < 0.0)
         {
           //printf("penetration\n");
           vars.d(3) = 0.0;
         }
         //printf("contact: %f\n", distance);
       }
       else
       {
         vars.d(0) = 0.0;
         vars.d(1) = 0.0;
         vars.d(2) = 0.0;
         vars.d(3) = _epsilon; 
         //outside so this values should be epsilon;
         //printf("no contact: %f\n", vars.d(3));
       }
   }
 
   if ( tarch::la::equals( t,0.0 ) )
   {
     Variables vars(Q);
 
     tarch::la::Vector<DIMENSIONS,double> myX( 
     x[0] - 0.06, 1.0-x[1] - 0.25 ); // translate
     myX *= static_cast<double>(Image.width);
     
     tarch::la::Vector<DIMENSIONS,int>    myIntX( 
     1.2*myX(0) , 1.2*myX(1) );  // scale
 
     double Energy = 0.1;
 
     if (myIntX(0) > 0 && 
     	 myIntX(0) < static_cast<int>(Image.width) &&
         myIntX(1) > 0 && 
         myIntX(1) < static_cast<int>(Image.height))
     {
       Energy += 1.0-
       Image.pixel_data[myIntX(1)*Image.width+myIntX(0)];
     }
 
     vars.rho() = 1.0;
     vars.E()   = Energy;
     vars.j(0,0,0);
   }
 }

\end{lstlisting}

\clearpage

\subsection{Update by Delta}

Lastly in a plotter solver/writter specified in the specification file, we write update delta by calling the iterate() routine. This routine automatically calls all DEM routines and updates all positions and collisions by $dt$ specified during initialisation.

\begin{lstlisting}
#include "DeltaUpdate.h"

#include "MyEulerSolver.h"

EulerFV::DeltaUpdate::DeltaUpdate() :
 exahype::plotters::FiniteVolumes2UserDefined::
 FiniteVolumes2UserDefined(){
}


void EulerFV::DeltaUpdate::plotPatch(
    const tarch::la::Vector<DIMENSIONS, double>& offsetOfPatch,
    const tarch::la::Vector<DIMENSIONS, double>& sizeOfPatch, 
    double* u,
    double timeStamp) {
}


void EulerFV::DeltaUpdate::startPlotting( double time) {
}


void EulerFV::DeltaUpdate::finishPlotting() {
#ifdef DeltaLIB
  EulerFV::MyEulerSolver::_deltaEngine.iterate();
#endif
}
\end{lstlisting}


\section{Experiments}

We test the code on ARCHER (Advanced Research Computing High End Resource). For the experiments we define two performance baselines. First we measure the runtime of the exahype Euler finite volume base code. The base code sets the baseline where Delta runtime is added. A second execution tests exahype with delta enabled. This sets the maximum time required to run Delta on the given setup. We see from Figure \ref{figure:exaRuntimes} on shared memory we get perfect scaling on an unoptimised scheme. The unoptimised scheme does not make any assumption on data/mesh locality. Lastly an optimised version falls in between the two basedline. The optimised version uses an octree to decompose the mesh into discrete chucks of search areas that is given a priori with hierarchical cubic cuts.  

\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{figures/runtime.png}
  \end{center}
  \begin{center}
    \includegraphics[width=0.8\textwidth]{figures/efficiency.png}
  \end{center}
  \caption{
    Run time and efficiency plots based on Intel KNL runs using the wind turbine model.
  }
  \label{figure:exaRuntimes}
\end{figure}


\bibliographystyle{plain}
\bibliography{paper}


\end{document}
