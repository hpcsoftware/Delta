#include "VFMesh.h"

#include <cmath>
#include <limits>
#include <iostream>


delta::VFMesh::VFMesh() {
  _max[0] = std::numeric_limits<iREAL>::min();
  _max[1] = std::numeric_limits<iREAL>::min();
  _max[2] = std::numeric_limits<iREAL>::min();

  _min[0] = std::numeric_limits<iREAL>::max();
  _min[1] = std::numeric_limits<iREAL>::max();
  _min[2] = std::numeric_limits<iREAL>::max();

  _flattenedXCoordinates = nullptr;
  _flattenedYCoordinates = nullptr;
  _flattenedZCoordinates = nullptr;
}


delta::VFMesh::~VFMesh() {
  freeFlattenedRepresentation();
}


void delta::VFMesh::flatten() {
  const int NumberOfFlattenedEntries = _triangle.size() * 3;
  _flattenedXCoordinates = new iREAL[ NumberOfFlattenedEntries ];
  _flattenedYCoordinates = new iREAL[ NumberOfFlattenedEntries ];
  _flattenedZCoordinates = new iREAL[ NumberOfFlattenedEntries ];

  int index = 0;
  for (auto p: _triangle) {
	_flattenedXCoordinates[index] = _vertex[ p.vertex[0] ].x[0];
	_flattenedYCoordinates[index] = _vertex[ p.vertex[0] ].x[1];
	_flattenedZCoordinates[index] = _vertex[ p.vertex[0] ].x[2];
	index++;

	_flattenedXCoordinates[index] = _vertex[ p.vertex[1] ].x[0];
	_flattenedYCoordinates[index] = _vertex[ p.vertex[1] ].x[1];
	_flattenedZCoordinates[index] = _vertex[ p.vertex[1] ].x[2];
	index++;

	_flattenedXCoordinates[index] = _vertex[ p.vertex[2] ].x[0];
	_flattenedYCoordinates[index] = _vertex[ p.vertex[2] ].x[1];
	_flattenedZCoordinates[index] = _vertex[ p.vertex[2] ].x[2];
	index++;
  }
}


void delta::VFMesh::finishedMeshBuildup() {
  freeFlattenedRepresentation();
  flatten();
}


void delta::VFMesh::freeFlattenedRepresentation() {
  if (_flattenedXCoordinates != nullptr) {
	delete[] _flattenedXCoordinates;
	_flattenedXCoordinates = nullptr;
  }
  if (_flattenedYCoordinates != nullptr) {
	delete[] _flattenedYCoordinates;
	_flattenedYCoordinates = nullptr;
  }
  if (_flattenedZCoordinates != nullptr) {
	delete[] _flattenedZCoordinates;
	_flattenedZCoordinates = nullptr;
  }

  _max[0] = std::numeric_limits<iREAL>::min();
  _max[1] = std::numeric_limits<iREAL>::min();
  _max[2] = std::numeric_limits<iREAL>::min();

  _min[0] = std::numeric_limits<iREAL>::max();
  _min[1] = std::numeric_limits<iREAL>::max();
  _min[2] = std::numeric_limits<iREAL>::max();
}


iREAL delta::VFMesh::getCentreX() const {
  return 0.5 * (_max[0] + _min[0]);
}


iREAL delta::VFMesh::getCentreY() const {
  return 0.5 * (_max[1] + _min[1]);
}


iREAL delta::VFMesh::getCentreZ() const {
  return 0.5 * (_max[2] + _min[2]);
}


iREAL delta::VFMesh::getBoundingSphereRadius() const {
  return 0.5 * std::sqrt(
    (_max[0]-_min[0]) * (_max[0]-_min[0])
	+
    (_max[1]-_min[1]) * (_max[1]-_min[1])
	+
    (_max[2]-_min[2]) * (_max[2]-_min[2])
  );
}


int delta::VFMesh::getNumberOfTriangles() const {
  return _triangle.size();
}


const iREAL* delta::VFMesh::getXCoordinates() const {
  return _flattenedXCoordinates;
}


const iREAL* delta::VFMesh::getYCoordinates() const  {
  return _flattenedYCoordinates;
}


const iREAL* delta::VFMesh::getZCoordinates() const  {
  return _flattenedZCoordinates;
}


int delta::VFMesh::addVertex(
  const iREAL x,
  const iREAL y,
  const iREAL z
) {
  _vertex.push_back( Vertex(x,y,z) );

  _min[0] = std::min( _min[0],x );
  _min[1] = std::min( _min[1],y );
  _min[2] = std::min( _min[2],z );

  _max[0] = std::max( _max[0],x );
  _max[1] = std::max( _max[1],y );
  _max[2] = std::max( _max[2],z );

  return _vertex.size()-1;
}



int delta::VFMesh::addTriangle(
  int index0,
  int index1,
  int index2
) {
  if (index0<0) {
    std::cerr << getOutputPrefix( "loadGTSFile(std::string)" ) <<  "triangle's vertex index is smaller 0" << std::endl;
  }
  if (index1<0) {
    std::cerr << getOutputPrefix( "loadGTSFile(std::string)" ) <<  "triangle's vertex index is smaller 0" << std::endl;
  }
  if (index2<0) {
    std::cerr << getOutputPrefix( "loadGTSFile(std::string)" ) <<  "triangle's vertex index is smaller 0" << std::endl;
  }
  if (index0>=_vertex.size()) {
    std::cerr << getOutputPrefix( "loadGTSFile(std::string)" ) <<  "triangle's vertex index is bigger than number of vertices (" << index0 << " vs " << _vertex.size() << ")" << std::endl;
  }
  if (index1>=_vertex.size()) {
    std::cerr << getOutputPrefix( "loadGTSFile(std::string)" ) <<  "triangle's vertex index is bigger than number of vertices (" << index1 << " vs " << _vertex.size() << ")" << std::endl;
  }
  if (index2>=_vertex.size()) {
    std::cerr << getOutputPrefix( "loadGTSFile(std::string)" ) <<  "triangle's vertex index is bigger than number of vertices (" << index2 << " vs " << _vertex.size() << ")" << std::endl;
  }
  _triangle.push_back( Triangle(index0,index1,index2) );
  return _triangle.size()-1;
}


delta::VFMesh::Vertex::Vertex(
  const iREAL x_,
  const iREAL y_,
  const iREAL z_
) {
  x[0] = x_;
  x[1] = y_;
  x[2] = z_;
}


delta::VFMesh::Triangle::Triangle( int a, int b, int c) {
  vertex[0] = a;
  vertex[1] = b;
  vertex[2] = c;
}

