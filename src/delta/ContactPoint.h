/*
 See license/copyright file at Delta's home www.peano-framework.org.

 Authors: Konstantinos Krestenitis, Tobias Weinzierl
 */
#ifndef _DELTA_CONTACTPOINT_H_
#define _DELTA_CONTACTPOINT_H_

#include "delta.h"

#include <string>
#include <cmath>
#include <limits>
#include <cmath>
#include <sstream>

namespace delta {
  struct ContactPoint;
}


/**
 * Simple struct representing a contact point
 */
struct delta::ContactPoint {
  /**
   * Position in space
   */
  iREAL 	x[3];

  /**
   * Normal onto next surface. Always normalised.
   */
  iREAL 	normal[3];

  /**
   * Distance to next surface along normal. We use the sign to indicate whether contact
   * point is inside (negative) or outside (positive) of the corresponding
   * shapes.
   */
  iREAL 	distance;

  /**
   * Allows us to tie a contact point to an object via an index
   */
  int 	    indexA;
  int 	    indexB;

  ContactPoint();
  ContactPoint(const ContactPoint& copy);

  /**
   * This constructor is given two points on two triangles that are close to
   * each other. The points are PA and PB. The operation determines the
   * contact point at x,y,z (which is half the distance between the two
   * points PA and PB) and the corresponding normal xN,yN,zN.
   */
  ContactPoint(
    const iREAL&  	xPA,
	const iREAL&  	yPA,
	const iREAL&  	zPA,
	int             indexA,

	const iREAL&  	xQB,
	const iREAL&  	yQB,
	const iREAL&  	zQB,
	int             indexB,

    const bool&     outside
  );

  ContactPoint(
    const iREAL PA[3],
	int         indexA,
	const iREAL QB[3],
	int         indexB,
    const bool&     outside
  );

  std::string toString() const;
};

#endif
