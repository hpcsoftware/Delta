/*
 See license/copyright file at Delta's home www.peano-framework.org.

 Authors: Konstantinos Krestenitis, Tobias Weinzierl
 */
#ifndef _DELTA_CONTACTDETECTION_MATH_H_
#define _DELTA_CONTACTDETECTION_MATH_H_


#include "delta.h"


namespace delta {
  /**
   * Dot product
   */
  iREAL dot(iREAL a[3], iREAL b[3]);

  /**
   * Cross product
   */
  void cross(iREAL a[3], iREAL b[3], iREAL result[3]);

  /**
   * Solves a 3x3 product via direct inversion. The matrix is given
   * column-wise. The column entries are changed in-situ.
   */
  void invert(iREAL col0[3], iREAL col1[3], iREAL col2[3]);

  /**
   * Determinant. Matrix is given column-wisely.
   */
  iREAL det(iREAL col0[3], iREAL col1[3], iREAL col2[3]);

  /**
   * Multiply a matrix given by its columns with a vector.
   */
  void mult(iREAL col0[3], iREAL col1[3], iREAL col2[3], iREAL x[3], iREAL result[3]);

  void normalise(iREAL x[3]);

  iREAL eukledianNorm(iREAL x[3]);
}

#endif
