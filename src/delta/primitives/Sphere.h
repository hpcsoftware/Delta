/*
 See license/copyright file at Delta's home www.peano-framework.org.

 Authors: Konstantinos Krestenitis, Tobias Weinzierl
 */
#ifndef _DELTA_PRIMITIVES_SPHERE_H_
#define _DELTA_PRIMITIVES_SPHERE_H_


#include "../delta.h"
#include "../Mesh.h"


namespace delta {
  namespace primitives {
    class Sphere;
  }
}



class delta::primitives::Sphere: public delta::Mesh {
  private:
	iREAL _centre[3];
	iREAL _radius;

  public:
	Sphere(
	  iREAL centre[3],
	  iREAL radius,
	  iREAL h
    );

	Sphere(
	  iREAL centreX,
	  iREAL centreY,
	  iREAL centreZ,
	  iREAL radius,
	  iREAL h
    );

	iREAL getCentreX() const override;
	iREAL getCentreY() const override;
	iREAL getCentreZ() const override;

	iREAL getBoundingSphereRadius() const override;

	int getNumberOfTriangles() const override;
	const iREAL* getXCoordinates() const override;
	const iREAL* getYCoordinates() const override;
	const iREAL* getZCoordinates() const override;
};


#endif
