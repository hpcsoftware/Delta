#include "Fault.h"
#include <iostream>
#include <cassert>
#include <cmath>


delta::primitives::Fault::Fault() {
  discretise();
}


delta::primitives::Fault::~Fault() {
  assert( _xCoordinates!=nullptr );
  assert( _yCoordinates!=nullptr );
  assert( _zCoordinates!=nullptr );

  if (_xCoordinates==nullptr) delete[] _xCoordinates;
  if (_yCoordinates==nullptr) delete[] _yCoordinates;
  if (_zCoordinates==nullptr) delete[] _zCoordinates;
}


iREAL delta::primitives::Fault::getCentreX() const {
  return 0.5;//_centre[0];
}


iREAL delta::primitives::Fault::getCentreY() const {
  return 0.5;//_centre[1];
}


iREAL delta::primitives::Fault::getCentreZ() const {
  return 0.5;//_centre[2];
}


iREAL delta::primitives::Fault::getBoundingSphereRadius() const {
  return std::sqrt(3)*1; //_h;
}


int delta::primitives::Fault::getNumberOfTriangles() const {
  return 8;
}


void delta::primitives::Fault::discretise() {
  _xCoordinates = new iREAL[3*getNumberOfTriangles()];
  _yCoordinates = new iREAL[3*getNumberOfTriangles()];
  _zCoordinates = new iREAL[3*getNumberOfTriangles()];

  _xCoordinates[0] = 0.5;
  _yCoordinates[0] = 0.3;
  _zCoordinates[0] = 0.3;
  _xCoordinates[1] = 0.7;
  _yCoordinates[1] = 0.2;
  _zCoordinates[1] = 0.1;
  _xCoordinates[2] = 0.5;
  _yCoordinates[2] = 0.7;
  _zCoordinates[2] = 0.3;

  _xCoordinates[3] = _xCoordinates[1];
  _yCoordinates[3] = _yCoordinates[1];
  _zCoordinates[3] = _zCoordinates[1];
  _xCoordinates[4] = _xCoordinates[2];
  _yCoordinates[4] = _yCoordinates[2];
  _zCoordinates[4] = _zCoordinates[2];
  _xCoordinates[5] = 0.65;
  _yCoordinates[5] = 0.6;
  _zCoordinates[5] = 0.15;

  _xCoordinates[6] = _xCoordinates[0];
  _yCoordinates[6] = _yCoordinates[0];
  _zCoordinates[6] = _zCoordinates[0];
  _xCoordinates[7] = 0.2;
  _yCoordinates[7] = 0.3;
  _zCoordinates[7] = 0.1;
  _xCoordinates[8] = _xCoordinates[2];
  _yCoordinates[8] = _yCoordinates[2];
  _zCoordinates[8] = _zCoordinates[2];

  _xCoordinates[9]  = _xCoordinates[7];
  _yCoordinates[9]  = _yCoordinates[7];
  _zCoordinates[9]  = _zCoordinates[7];
  _xCoordinates[10] = _xCoordinates[8];
  _yCoordinates[10] = _yCoordinates[8];
  _zCoordinates[10] = _zCoordinates[8];
  _xCoordinates[11] = 0.2;
  _yCoordinates[11] = 0.6;
  _zCoordinates[11] = 0.1;

  _xCoordinates[12] = _xCoordinates[0];
  _yCoordinates[12] = _yCoordinates[0];
  _zCoordinates[12] = _zCoordinates[0];
  _xCoordinates[13] = 0.5;
  _yCoordinates[13] = 0.3;
  _zCoordinates[13] = 0.5;
  _xCoordinates[14] = _xCoordinates[2];
  _yCoordinates[14] = _yCoordinates[2];
  _zCoordinates[14] = _zCoordinates[2];

  _xCoordinates[15] = _xCoordinates[14];
  _yCoordinates[15] = _yCoordinates[14];
  _zCoordinates[15] = _zCoordinates[14];
  _xCoordinates[16] = 0.5;
  _yCoordinates[16] = 0.75;
  _zCoordinates[16] = 0.5;
  _xCoordinates[17] = _xCoordinates[13];
  _yCoordinates[17] = _yCoordinates[13];
  _zCoordinates[17] = _zCoordinates[13];

  _xCoordinates[18] = _xCoordinates[17];
  _yCoordinates[18] = _yCoordinates[17];
  _zCoordinates[18] = _zCoordinates[17];
  _xCoordinates[19] = 0.6;
  _yCoordinates[19] = 0.4;
  _zCoordinates[19] = 0.85;
  _xCoordinates[20] = _xCoordinates[16];
  _yCoordinates[20] = _yCoordinates[16];
  _zCoordinates[20] = _zCoordinates[16];

  _xCoordinates[21] = _xCoordinates[20];
  _yCoordinates[21] = _yCoordinates[20];
  _zCoordinates[21] = _zCoordinates[20];
  _xCoordinates[22] = 0.65;
  _yCoordinates[22] = 0.8;
  _zCoordinates[22] = 0.85;
  _xCoordinates[23] = _xCoordinates[19];
  _yCoordinates[23] = _yCoordinates[19];
  _zCoordinates[23] = _zCoordinates[19];
}


const iREAL* delta::primitives::Fault::getXCoordinates() const {
  assert( _xCoordinates!=nullptr );
  assert( _yCoordinates!=nullptr );
  assert( _zCoordinates!=nullptr );
  return _xCoordinates;
}


const iREAL* delta::primitives::Fault::getYCoordinates() const {
  assert( _xCoordinates!=nullptr );
  assert( _yCoordinates!=nullptr );
  assert( _zCoordinates!=nullptr );
  return _yCoordinates;
}


const iREAL* delta::primitives::Fault::getZCoordinates() const {
  assert( _xCoordinates!=nullptr );
  assert( _yCoordinates!=nullptr );
  assert( _zCoordinates!=nullptr );
  return _zCoordinates;
}
