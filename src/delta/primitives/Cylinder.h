/*
 See license/copyright file at Delta's home www.peano-framework.org.

 Authors: Konstantinos Krestenitis, Tobias Weinzierl
 */
#ifndef _DELTA_PRIMITIVES_CYLINDER_H_
#define _DELTA_PRIMITIVES_CYLINDER_H_


#include "../delta.h"
#include "../Mesh.h"


namespace delta {
  namespace primitives {
    class Cylinder;
  }
}


/**
 * A cylinder
 *
 */
class delta::primitives::Cylinder: public delta::Mesh {
  private:
	const iREAL _centre[3];
	const iREAL _radius;
	const iREAL _h;
	const iREAL _maxZ;
	const iREAL _minZ;

	iREAL* _xCoordinates;
	iREAL* _yCoordinates;
	iREAL* _zCoordinates;

	/**
	 * @image html Cylinder.png
	 */
	void discretise();

    int getNumberOfTrianglesPerCircle() const;
  public:
	/**
	 * Uses C++14's constructor delegation.
	 *
	 * @see Cylinder()
	  double minZ = -1.0,
	  double maxZ =  1.0,
	 */
	Cylinder(
	  iREAL centre[3],
	  iREAL radius,
	  iREAL minZ,
	  iREAL maxZ,
	  iREAL h
    );

	Cylinder(
	  iREAL centreX,
	  iREAL centreY,
	  iREAL centreZ,
	  iREAL radius,
	  iREAL minZ,
	  iREAL maxZ,
	  iREAL h
    );

	virtual ~Cylinder();

	iREAL getCentreX() const override;
	iREAL getCentreY() const override;
	iREAL getCentreZ() const override;

	iREAL getBoundingSphereRadius() const override;

	/**
	 * You need at least three lines to approximate a circle. Lets assume
	 * that we have k lines to approximate a circle. Per line, we need two
	 * triangles to span a rectangular face.
	 *
	 * @image html Cylinder.png
	 */
	int getNumberOfTriangles() const override;
	const iREAL* getXCoordinates() const override;
	const iREAL* getYCoordinates() const override;
	const iREAL* getZCoordinates() const override;
};


#endif
