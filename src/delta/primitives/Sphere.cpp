#include "Sphere.h"
#include <iostream>
#include <cassert>


delta::primitives::Sphere::Sphere(
  iREAL centre[3],
  iREAL radius,
  iREAL h
):
  Sphere(
    centre[0], centre[1], centre[2],
	radius, h
  ) {
}


delta::primitives::Sphere::Sphere(
  iREAL centreX,
  iREAL centreY,
  iREAL centreZ,
  iREAL radius,
  iREAL h
):
  _centre{ centreX, centreY, centreZ },
  _radius(radius) {

  #if DeltaDebug>=1
  std::cout << delta::getOutputPrefix("delta::primitives::Sphere::Sphere") << "create sphere around ("
		    << centreX << ","
		    << centreY << ","
		    << centreZ << ")^T with radius " << radius << std::endl;
  #endif
}


iREAL delta::primitives::Sphere::getCentreX() const {
  return _centre[0];
}


iREAL delta::primitives::Sphere::getCentreY() const {
  return _centre[1];
}


iREAL delta::primitives::Sphere::getCentreZ() const {
  return _centre[2];
}


iREAL delta::primitives::Sphere::getBoundingSphereRadius() const {
  return _radius;
}


int delta::primitives::Sphere::getNumberOfTriangles() const {
  assert(false);
  return 0;
}


const iREAL* delta::primitives::Sphere::getXCoordinates() const {
  assert(false);
  return nullptr;
}


const iREAL* delta::primitives::Sphere::getYCoordinates() const {
  assert(false);
  return nullptr;
}


const iREAL* delta::primitives::Sphere::getZCoordinates() const {
  assert(false);
  return nullptr;
}
