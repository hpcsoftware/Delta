/*
 See license/copyright file at Delta's home www.peano-framework.org.

 Authors: Tobias Weinzierl
 */
#ifndef _DELTA_PRIMITIVES_FAULT_H_
#define _DELTA_PRIMITIVES_FAULT_H_


#include "../delta.h"
#include "../Mesh.h"


namespace delta {
  namespace primitives {
    class Fault;
  }
}


/**
 * A cube
 */
class delta::primitives::Fault: public delta::Mesh {
  private:
	iREAL* _xCoordinates;
	iREAL* _yCoordinates;
	iREAL* _zCoordinates;

	void discretise();
  public:
	/**
	 * Uses C++14's constructor delegation.
	 *
	 */
	Fault();

	virtual ~Fault();

	iREAL getCentreX() const override;
	iREAL getCentreY() const override;
	iREAL getCentreZ() const override;

	iREAL getBoundingSphereRadius() const override;

	int getNumberOfTriangles() const override;
	const iREAL* getXCoordinates() const override;
	const iREAL* getYCoordinates() const override;
	const iREAL* getZCoordinates() const override;
};


#endif
