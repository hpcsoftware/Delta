/*
 See license/copyright file at Delta's home www.peano-framework.org.

 Authors: Konstantinos Krestenitis, Tobias Weinzierl
 */
#ifndef _DELTA_PRIMITIVES_CUBE_H_
#define _DELTA_PRIMITIVES_CUBE_H_


#include "../delta.h"
#include "../Mesh.h"


namespace delta {
  namespace primitives {
    class Cube;
  }
}


/**
 * A cube
 */
class delta::primitives::Cube: public delta::Mesh {
  private:
	const iREAL _centre[3];
	const iREAL _h;

	iREAL* _xCoordinates;
	iREAL* _yCoordinates;
	iREAL* _zCoordinates;

	void discretise();
  public:
	/**
	 * Uses C++14's constructor delegation.
	 *
	 */
	Cube(
	  iREAL centre[3],
	  iREAL h
    );

	Cube(
	  iREAL centreX,
	  iREAL centreY,
	  iREAL centreZ,
	  iREAL h
    );

	virtual ~Cube();

	iREAL getCentreX() const override;
	iREAL getCentreY() const override;
	iREAL getCentreZ() const override;

	iREAL getBoundingSphereRadius() const override;

	int getNumberOfTriangles() const override;
	const iREAL* getXCoordinates() const override;
	const iREAL* getYCoordinates() const override;
	const iREAL* getZCoordinates() const override;
};


#endif
