/*
 See license/copyright file at Delta's home www.peano-framework.org.

 Authors: Konstantinos Krestenitis, Tobias Weinzierl
 */
#define validateEquals(lhs,rhs) if (lhs!=rhs) { \
    std::cerr << "  equality test failed. lhs=" << lhs << ", rhs=" << rhs << std::endl \
              << "  file: " << __FILE__ << " \t line: " << __LINE__ << std::endl \
              << "  statement: " << #lhs << "==" << #rhs << std::endl; \
    return 1;\
  }


#define validate(booleanExpr) if (!(booleanExpr)) { \
    std::cerr << "  boolean test failed " << std::endl \
              << "  file: " << __FILE__ << " \t line: " << __LINE__ << std::endl \
              << "  statement: " << #booleanExpr << std::endl; \
    return 1;\
  }

