/*
 See LICENSE file in root of repository

 This file is only to be compiled if you build the Delta test suite. It is the
 unit test driver.

 Authors: Tobias Weinzierl
 */
#include "delta.h"

#include "faultscenario.h"

#include "contactdetection/tests/sphere.h"

#include <iostream>


int main() {
  int errors = 0;

  errors += delta::contactdetection::tests::testPointToTriangle();
  errors += delta::tests::faultScenarioTest();
  errors += delta::tests::loadGTSFileTest();

  if (errors>0) {
	std::cerr << delta::getOutputPrefix("main") << "unit tests failed: " << errors << " error(s)" << std::endl;
  }
  else {
    std::cout << delta::getOutputPrefix("main") << "tests passed without any failures" << std::endl;
  }

  return 0;
}
