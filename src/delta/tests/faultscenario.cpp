#include "faultscenario.h"
#include "TestMacros.h"

#include "delta.h"
#include "delta/primitives/Fault.h"
#include "delta/contactdetection/sphere.h"
#include "delta/contactdetection/filter.h"
#include "delta/io/vtk.h"
#include "delta/io/gts.h"


#include <iostream>


int delta::tests::faultScenarioTest() {
  std::cout << getOutputPrefix("delta::tests::faultScenarioTest()") << "start test delta::tests::faultScenarioTest()" << std::endl;

  delta::Mesh* myMesh = new delta::primitives::Fault();

  std::vector<delta::ContactPoint> contactPoints =
    delta::contactdetection::sphereToTriangle(
     0.55,0.5,0.5,0.0,-1,
	 myMesh->getNumberOfTriangles(),
	 myMesh->getXCoordinates(),
     myMesh->getYCoordinates(),
     myMesh->getZCoordinates(),
     nullptr,
     0.2
   );

  validateEquals( contactPoints.size(), 8 );

  std::vector<delta::ContactPoint> orderedContactPoints =
    delta::contactdetection::filter( contactPoints, 0.1 );

  delta::io::writeVTK(
    myMesh->getNumberOfTriangles(),
	myMesh->getXCoordinates(),
	myMesh->getYCoordinates(),
	myMesh->getZCoordinates(),
	"delta-test-faultScenarioTest-geometry.vtk"
  );
  delta::io::writeVTK( orderedContactPoints[0], "delta-test-faultScenarioTest-closestPoint.vtk" );
  delta::io::writeVTK( contactPoints, "delta-test-faultScenarioTest-allPoints.vtk" );

  delete myMesh;

  return 0;
}



int delta::tests::loadGTSFileTest() {
  std::cout << getOutputPrefix("delta::tests::loadGTSFileTest()") << "start test delta::tests::loadGTSFileTest()" << std::endl;

  delta::Mesh* myMesh = delta::io::loadGTSFile( "triangular_mesh.gts" );

  delta::io::writeVTK(
    myMesh->getNumberOfTriangles(),
	myMesh->getXCoordinates(),
	myMesh->getYCoordinates(),
	myMesh->getZCoordinates(),
	"delta-test-loadGTSFileTest-geometry.vtk"
  );

  delete myMesh;

  return 0;
}
