/*
 See license/copyright file at Delta's home www.peano-framework.org.

 Authors: Konstantinos Krestenitis, Tobias Weinzierl
 */
#ifndef _DELTA_TESTS_FAULT_SCENARIO_H_
#define _DELTA_TESTS_FAULT_SCENARIO_H_

namespace delta {
  namespace tests {
    /**
     * Very simple test which creates our dummy/fake fault geometry,
     * runs some distance calculations, i.e. tries to find for a point
     * the distance to the geometry, and then dumps the output as
     * vtk file.
     *
     * @image html delta-test-faultScenarioTest-geometry.png
     *
     * The function can be seen as both a real test and also a howto
     * how to use Delta.
     *
     * <h2> Algorithmic steps </h2>
     *
     * We first create an instance of Fault. I use a Javaish programming
     * style here, i.e. I use pointers. This certainly is not necessary
     * here (we could work directly with Fault), but it is the pattern we
     * find most often in our codes.
     *
     * We next would like to know for point (0.55,0.5,0.5) what the nearest
     * triangle is. Technically, a point is a degenerated sphere. So we use
     * the routine delta::contactdetection::sphereToTriangle(). Our sphere's
     * radius is zero, as we are not really interested in sphere-to-triangle
     * checks. We wanna have point-to-sphere. Any Delta routine accepts
     * indices. They basically allow us to give each triangle/sphere an
     * index. If the code finds a contact point/shortest distance, it augments
     * these points with these indices, so you know which triangles have
     * collided. We don't need this feature here and thus pass -1 or a
     * nullptr, respectively.
     *
     * Delta is originally written to detect collisions/overlaps between
     * meshes. We kind of hijack this idea here: The collision routines all
     * allow us to wrap the geometries into a small epsilon halo. If these
     * halos overlap, we call two geometries to be in contact: We kind of
     * blow up the geometries by epsilon and then look where these blow up
     * areas do overlap.
     *
     * As a result, every collision check can return a set of contact points.
     * I use a C++ vector, i.e. an array, as data structure here. There's no
     * particular ordering within this array (actually, the order of contact
     * points correlates to the ordering of the geometry triangles). So we
     * take all identified contact points and pass them into
     * delta::contactdetection::filter(). In the present example, the original
     * data yields eight closest triangles, i.e. eight triangles intersect
     * with the epsilon environment of around our query point. Once we have
     * sorted the contact points, we only use the first point: We are
     * interested in the closest point, so we drop all other contacts.
     *
     *
     * We finally dump both the contact points and the geometry. For this,
     * we rely on routines from the namespace delta::io.
     */
    int faultScenarioTest();
    int loadGTSFileTest();
  }
}

#endif
