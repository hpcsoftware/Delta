#include "filter.h"
#include "../math.h"

#include <iostream>


std::vector<delta::ContactPoint> delta::contactdetection::filter( const std::vector<delta::ContactPoint>& contactPoints, iREAL h) {
  std::vector<delta::ContactPoint> manualCopy;
  manualCopy.insert( manualCopy.begin(), contactPoints.begin(), contactPoints.end() );

  return filter( std::move(manualCopy),h );
}


std::vector<delta::ContactPoint> delta::contactdetection::filter( std::vector<delta::ContactPoint>&& contactPoints, iREAL h) {
  if (contactPoints.empty()) {
	return contactPoints;
  }

  std::vector<delta::ContactPoint> result;

  bool hasChanged = true;
  while (hasChanged) {
    hasChanged = false;
    for (int i=1; i<contactPoints.size(); i++) {
      if (
        (contactPoints[i].distance<0 and contactPoints[i-1].distance>0)
		or
        (contactPoints[i].distance<0 and contactPoints[i-1].distance<0 and std::abs(contactPoints[i].distance) < std::abs(contactPoints[i-1].distance))
		or
        (contactPoints[i].distance>0 and contactPoints[i-1].distance>0 and std::abs(contactPoints[i].distance) < std::abs(contactPoints[i-1].distance))
      ) {
    	 ContactPoint tmp = contactPoints[i];
    	 contactPoints[i] = contactPoints[i-1];
    	 contactPoints[i-1] = tmp;
    	 hasChanged = true;
      }
    }
  }


  #if DeltaDebug>=2
  std::cout << delta::getOutputPrefix("delta::filter") << "closest contact point is " << contactPoints[0].toString() << std::endl;
  #endif

  result.push_back( contactPoints[0] );

  for (auto in: contactPoints) {
    bool foundTooClosePoint = false;

    for (auto out: result) {
      iREAL distance[] = {in.x[0]-out.x[0],in.x[1]-out.x[1],in.x[2]-out.x[2]};

      if ( delta::dot(distance,distance)<h*h ) {
        foundTooClosePoint = true;
      }
    }

    if (!foundTooClosePoint) {
      result.push_back(in);
    }
  }

  return result;
}
