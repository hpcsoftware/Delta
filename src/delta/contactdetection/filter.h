/*
 See license/copyright file at Delta's home www.peano-framework.org.

 Authors: Konstantinos Krestenitis, Tobias Weinzierl
 */
#ifndef _DELTA_CONTACTDETECTION_FILTER_H_
#define _DELTA_CONTACTDETECTION_FILTER_H_


#include "../delta.h"
#include "../ContactPoint.h"
#include <vector>


namespace delta {
  namespace contactdetection {
    /**
     * Sort the contact points along their distance and filter out/drop all of those
     * guys which are closer than h. This is not an optimised code variant, so we
     * should tune it some day soon. The implemented algorithm consists of two steps.
     *
     * <h2> Step 1: Sort along importance </h2>
     *
     * The filter employs a simple bubble sort. Here's the filter mechanism:
     * - Negative (inside) points are more important than all other points.
     * - If the points' distance attribute have the same sign, i.e. two points are
     *   both inside or outside, respectively, then the one with the smaller
     *   absolute distance value is the more important point.
     *
     * <h2> Step 2: Eliminate points that are too close </h2>
     *
     * We assume that points which are closer than the passed h are not
     * representing different contact points. So we eliminate them along the
     * sorting: The closest point in the result is by definition a valid point.
     * All points within an h environment are eliminated. The closest point
     * to the first point that is outside of the h environment is a valid
     * contact point. We continue recursively with the remaining points.
     *
     * <h2> Using the move argument </h2>
     *
     * Most codes wrap filter directly around the contact detection. They use
     * code lines alike
     *
     * <pre>
  std::vector<delta::ContactPoint> contactPoints =
    delta::contactdetection::filter(
    delta::contactdetection::sphereToTriangle(
     0.55,0.5,0.5,0.0,-1,
	 myMesh->getNumberOfTriangles(),
	 myMesh->getXCoordinates(),
     myMesh->getYCoordinates(),
     myMesh->getZCoordinates(),
     nullptr,
     0.2
   ));
       </pre>
     *
     * As I use the move parameter, this snippet works without generating any
     * temporary data structure in memory.
     */
    std::vector<delta::ContactPoint> filter( std::vector<delta::ContactPoint>&&  contactPoints, iREAL h);

    std::vector<delta::ContactPoint> filter( const std::vector<delta::ContactPoint>& contactPoints, iREAL h);
  }
}

#endif
