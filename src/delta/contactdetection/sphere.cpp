#include "sphere.h"
#include "../math.h"
#include "../ContactPoint.h"
#include <iostream>
#include <cmath>
#include <algorithm>
#include "../../config.h"

namespace {
  iREAL myClamp( iREAL one, iREAL two, iREAL three) {
    #ifdef HAVE_CXX17
	return std::clamp( one, two, three );
    #else
	return std::max(std::min(one, three), two );
    #endif
  }
}


std::vector<delta::ContactPoint> delta::contactdetection::sphereToSphere(
  const iREAL   xCoordinatesOfSphereA,
  const iREAL   yCoordinatesOfSphereA,
  const iREAL   zCoordinatesOfSphereA,
  const iREAL   radiusA,
  int           indexA,

  const iREAL   xCoordinatesOfSphereB,
  const iREAL   yCoordinatesOfSphereB,
  const iREAL   zCoordinatesOfSphereB,
  const iREAL   radiusB,
  int           indexB,

  const iREAL   epsilon
) {
  std::vector<delta::ContactPoint> result;

  iREAL xd = xCoordinatesOfSphereB-xCoordinatesOfSphereA;
  iREAL yd = yCoordinatesOfSphereB-yCoordinatesOfSphereA;
  iREAL zd = zCoordinatesOfSphereB-zCoordinatesOfSphereA;

  iREAL distance = std::sqrt(xd*xd + yd*yd + zd*zd);

  iREAL xNormal = (1.0/distance) * xd;
  iREAL yNormal = (1.0/distance) * yd;
  iREAL zNormal = (1.0/distance) * zd;

  if (distance <= radiusA + radiusB + 2 * epsilon) {
    delta::ContactPoint newContactPoint(
      xCoordinatesOfSphereA + xNormal * radiusA,
      yCoordinatesOfSphereA + yNormal * radiusA,
      zCoordinatesOfSphereA + zNormal * radiusA,
	  indexA,

      xCoordinatesOfSphereB - xNormal * radiusB,
      yCoordinatesOfSphereB - yNormal * radiusB,
      zCoordinatesOfSphereB - zNormal * radiusB,
	  indexB,

	  distance > radiusA + radiusB
    );
	result.push_back( newContactPoint );
  }

  #if DeltaDebug>=2
  std::cout << delta::getOutputPrefix("delta::contactdetection::sphereToSphere") << "found " << result.size() << " contact point(s) "
		    << std::endl;
  #endif

  return result;
}


namespace {
  /**
   * This is a standard routine as you find it in any CG textbook. We assume
   * that a triangle is given and that the first vertex of this triangle is
   * found in (0,0,0). So all data is properly translated. To span the
   * triangle, we are given its two edges side1 and side2. Finally, we are
   * given a point (called, for historic reasons, centreToTriangleOrigin).
   * We now return the closest point to the latter in barycentric coordinates.
   */
  std::pair<iREAL,iREAL> getBarycentricCoordinatesOfClosestPointOnTriangle(
    iREAL  triangleSide1[3],
    iREAL  triangleSide2[3],
    iREAL  centreToTriangleOrigin[3]
  ) {
    iREAL a = ::delta::dot(triangleSide1,triangleSide1);
    iREAL b = ::delta::dot(triangleSide1,triangleSide2);
    iREAL c = ::delta::dot(triangleSide2,triangleSide2);
    iREAL d = ::delta::dot(triangleSide1,centreToTriangleOrigin);
    iREAL e = ::delta::dot(triangleSide2,centreToTriangleOrigin);

    iREAL det = a*c - b*b;
    iREAL s = b*e - c*d;
    iREAL t = b*d - a*e;

    if ( s + t < det ) {
      if ( s < 0 ) {
        if ( t < 0 ) {
          if ( d < 0 ) {
            s = myClamp( -d/a, static_cast<iREAL>(0), static_cast<iREAL>(1) );
            t = 0;
          }
          else {
            s = 0;
            t = myClamp( -e/c, static_cast<iREAL>(0), static_cast<iREAL>(1) );
          }
        }
        else {
          s = 0;
          t = myClamp( -e/c, static_cast<iREAL>(0), static_cast<iREAL>(1) );
        }
      }
      else if ( t < 0 ) {
        s = myClamp( -d/a, static_cast<iREAL>(0), static_cast<iREAL>(1) );
        t = 0;
      }
      else {
        s /= det;
        t /= det;
      }
    }
    else {
      if ( s < 0 ) {
        iREAL tmp0 = b+d;
        iREAL tmp1 = c+e;
        if ( tmp1 > tmp0 ) {
          iREAL numer = tmp1 - tmp0;
          iREAL denom = a-2*b+c;
          s = myClamp( numer/denom, static_cast<iREAL>(0), static_cast<iREAL>(1) );
          t = 1-s;
        }
        else {
          t = myClamp( -e/c, static_cast<iREAL>(0), static_cast<iREAL>(1) );
          s = 0;
        }
      }
      else if ( t < 0 ) {
        if ( a+d > b+e ) {
          iREAL numer = c+e-b-d;
          iREAL denom = a-2*b+c;
          s = myClamp( numer/denom, static_cast<iREAL>(0), static_cast<iREAL>(1) );
          t = 1-s;
        }
        else {
          s = myClamp( -e/c, static_cast<iREAL>(0), static_cast<iREAL>(1) );
          t = 0;
        }
      }
      else {
        iREAL numer = c+e-b-d;
        iREAL denom = a-2*b+c;
        s = myClamp( numer/denom, static_cast<iREAL>(0), static_cast<iREAL>(1) );
        t = 1 - s;
      }
    }
    return std::pair<iREAL,iREAL>(s,t);
  }


  void insertContactPoint(
    iREAL   xCoordinatesOfSphere,
    iREAL   yCoordinatesOfSphere,
    iREAL   zCoordinatesOfSphere,
    iREAL   radius,
	int     indexOfSphere,
    iREAL   closestPointOnTriangle[3],
	int     indexOfTriangle,
    const iREAL   epsilon,
	std::vector<delta::ContactPoint>& listOfContactPoints
  ) {
    iREAL distanceToCentreOfSphere[] = {
      closestPointOnTriangle[0] - xCoordinatesOfSphere,
	  closestPointOnTriangle[1] - yCoordinatesOfSphere,
	  closestPointOnTriangle[2] - zCoordinatesOfSphere
    };
    iREAL distance = std::sqrt( ::delta::dot(distanceToCentreOfSphere,distanceToCentreOfSphere) );
    if (distance<radius + 2*epsilon) {
      iREAL contactPointOnSphere[] = {
    	xCoordinatesOfSphere + distanceToCentreOfSphere[0] / distance * radius,
    	yCoordinatesOfSphere + distanceToCentreOfSphere[1] / distance * radius,
    	zCoordinatesOfSphere + distanceToCentreOfSphere[2] / distance * radius
      };

      #if DeltaDebug>=2
      std::cout << delta::getOutputPrefix("::insertContactPoint") << "distance to sphere=" << distanceToCentreOfSphere[0] << "x" << distanceToCentreOfSphere[1]
                << "x" << distanceToCentreOfSphere[2]
          << std::endl;
      std::cout << delta::getOutputPrefix("::insertContactPoint") << "point on sphere=" << contactPointOnSphere[0] << "x" << contactPointOnSphere[1]
                << "x" << contactPointOnSphere[2]
          << std::endl;
      #endif

      listOfContactPoints.push_back(
        ::delta::ContactPoint(
          closestPointOnTriangle,
		  indexOfTriangle,
		  contactPointOnSphere,
		  indexOfSphere,
		  distance>radius
		));
    }
  }
}


std::vector<delta::ContactPoint> delta::contactdetection::sphereToTriangle(
  const iREAL   xCoordinatesOfSphere,
  const iREAL   yCoordinatesOfSphere,
  const iREAL   zCoordinatesOfSphere,
  const iREAL   radius,
  const int     indexOfSphere,
  const int	    numberOfTriangles,
  const iREAL*  xCoordinates,
  const iREAL*  yCoordinates,
  const iREAL*  zCoordinates,
  const int*    indices,
  const iREAL   epsilon
) {
  #if DeltaDebug>=2
  std::cout << delta::getOutputPrefix("delta::contactdetection::sphereToTriangle") << "sphere=" << xCoordinatesOfSphere << "x" << yCoordinatesOfSphere<< "x" << zCoordinatesOfSphere << std::endl;
  #endif

  std::vector<delta::ContactPoint> result;

  for (int triangle=0; triangle<numberOfTriangles; triangle++) {
    //
    // ======== New Code ========
    //
    iREAL triangleSide1[] = {
      xCoordinates[triangle*3+1] - xCoordinates[triangle*3],
      yCoordinates[triangle*3+1] - yCoordinates[triangle*3],
      zCoordinates[triangle*3+1] - zCoordinates[triangle*3]
    };
    iREAL triangleSide2[] = {
      xCoordinates[triangle*3+2] - xCoordinates[triangle*3],
      yCoordinates[triangle*3+2] - yCoordinates[triangle*3],
      zCoordinates[triangle*3+2] - zCoordinates[triangle*3]
    };
    iREAL centreToTriangleOrigin[] = {
      xCoordinates[triangle*3] - xCoordinatesOfSphere,
      yCoordinates[triangle*3] - yCoordinatesOfSphere,
      zCoordinates[triangle*3] - zCoordinatesOfSphere
    };

    std::pair< iREAL, iREAL > st = getBarycentricCoordinatesOfClosestPointOnTriangle(
      triangleSide1, triangleSide2, centreToTriangleOrigin
    );

    // Construct closest point to sphere centre on triangle
    iREAL closestPointOnTriangle1[] = {
      xCoordinates[triangle*3] + st.first * triangleSide1[0] + st.second * triangleSide2[0],
      yCoordinates[triangle*3] + st.first * triangleSide1[1] + st.second * triangleSide2[1],
      zCoordinates[triangle*3] + st.first * triangleSide1[2] + st.second * triangleSide2[2]
    };

    insertContactPoint(
      xCoordinatesOfSphere,
      yCoordinatesOfSphere,
      zCoordinatesOfSphere,
      radius,
	  indexOfSphere,
      closestPointOnTriangle1,
	  indices != nullptr ? indices[triangle] : -1,
      epsilon,
  	  result
    );

/*
    st = getBarycentricCoordinatesOfClosestPointOnTriangle(
      triangleSide2, triangleSide1, centreToTriangleOrigin
    );

    // Construct closest point to sphere centre on triangle
    iREAL closestPointOnTriangle2[] = {
      xCoordinates[triangle*3] + st.first * triangleSide2[0] + st.second * triangleSide1[0],
      yCoordinates[triangle*3] + st.first * triangleSide2[1] + st.second * triangleSide1[1],
      zCoordinates[triangle*3] + st.first * triangleSide2[2] + st.second * triangleSide1[2]
    };

    insertContactPoint(
      xCoordinatesOfSphere,
      yCoordinatesOfSphere,
      zCoordinatesOfSphere,
      radius,
      closestPointOnTriangle2,
      epsilon,
  	  result
    );
*/
  }

  #if DeltaDebug>=2
  std::cout << delta::getOutputPrefix("delta::contactdetection::sphereToTriangle") << "found " << result.size() << " contact point(s) "
		    << std::endl;
  #endif
  return result;
}


