/*
 See license/copyright file at Delta's home www.peano-framework.org.

 Authors: Konstantinos Krestenitis, Tobias Weinzierl
 */
#ifndef _DELTA_CONTACTDETECTION_SPHERE_H_
#define _DELTA_CONTACTDETECTION_SPHERE_H_


#include "../delta.h"
#include <vector>


namespace delta {
  /**
   * Forward declaration
   */
  class ContactPoint;

  namespace contactdetection {
    /**
     * Compare contact point between two spheres.
     *
     * <h2> Implementation </h2>
     *
     * All vectors point from A to B.
     *
     * @param indexA Index that will be stored in the contact point object
     */
    std::vector<delta::ContactPoint> sphereToSphere(
		const iREAL   xCoordinatesOfSphereA,
		const iREAL   yCoordinatesOfSphereA,
		const iREAL   zCoordinatesOfSphereA,
		const iREAL   radiusA,
        const int     indexA,

		const iREAL   xCoordinatesOfSphereB,
		const iREAL   yCoordinatesOfSphereB,
		const iREAL   zCoordinatesOfSphereB,
		const iREAL   radiusB,
        const int     indexB,

		const iREAL   epsilon
	);

    /**
     * Compute distance from sphere, i.e. center of sphere, to triangle.
     *
     * This implementation closely follows https://www.geometrictools.com/Documentation/DistancePoint3Triangle3.pdf.
     * The big difference to all the books from computer graphics is that I
     * work without any assumption on the triangle orientation here.
     *
     * @img html sphere.png
     * @param indices indices of the triangles. Pass a nullptr if you are not
     *                interested in indices.
     */
    std::vector<delta::ContactPoint> sphereToTriangle(
		const iREAL   xCoordinatesOfSphere,
		const iREAL   yCoordinatesOfSphere,
		const iREAL   zCoordinatesOfSphere,
		const iREAL   radius,
		const int     indexSphere,

		const int      numberOfTriangles,
		const iREAL*   xCoordinates,
		const iREAL*   yCoordinates,
		const iREAL*   zCoordinates,
		const int*     indices,

		const iREAL   epsilon
	);
  }
}


#endif
