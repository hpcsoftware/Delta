/*
 See license/copyright file at Delta's home www.peano-framework.org.

 Authors: Konstantinos Krestenitis, Tobias Weinzierl
 */
#ifndef _DELTA_CONTACTDETECTION_TESTS_SPHERE_H_
#define _DELTA_CONTACTDETECTION_TESTS_SPHERE_H_


#include "../../delta.h"

namespace delta {
  namespace contactdetection {
    namespace tests {
      /**
       * We have a sphere at 0,0,1 with radius 0.5.
       * The triangle is spanned by (0,0,0), (1,0,0) and (0,1,0).
       * So it is clear that the closest point (if there's one) has
       * to be (0,0,0.25).
       */
      int testPointToTriangle();
    }
  }
}

#endif
