#include "sphere.h"
#include "../sphere.h"
#include "../../ContactPoint.h"
#include <iostream>
#include "../../tests/TestMacros.h"


int delta::contactdetection::tests::testPointToTriangle() {
  std::cout << getOutputPrefix("delta::contactdetection::tests::testPointToTriangle()") << "start test delta::contactdetection::tests::testPointToTriangle()" << std::endl;

  iREAL xCoordinatesOfSphere = 0.0;
  iREAL yCoordinatesOfSphere = 0.0;
  iREAL zCoordinatesOfSphere = 1.0;
  iREAL radius               = 0.5;
  iREAL xCoordinates[]  = {0.0, 1.0, 0.0};
  iREAL yCoordinates[]  = {0.0, 0.0, 1.0};
  iREAL zCoordinates[]  = {0.0, 0.0, 0.0};
  int   	    numberOfTriangles = 1;
  iREAL epsilon = 2.0;

  std::vector<delta::ContactPoint> contactPoints;
  contactPoints =
    delta::contactdetection::sphereToTriangle(
	  xCoordinatesOfSphere,
	  yCoordinatesOfSphere,
	  zCoordinatesOfSphere,
	  radius,
	  -1,
	  numberOfTriangles,
	  xCoordinates,
	  yCoordinates,
	  zCoordinates,
	  nullptr,
	  epsilon
	);

  validateEquals( contactPoints.size(),1 );

  if (contactPoints[0].x[0]!=0.0) return 1;
  if (contactPoints[0].x[1]!=0.0) return 1;
  if (contactPoints[0].x[2]!=0.25) return 1;

  xCoordinatesOfSphere =  0.0;
  yCoordinatesOfSphere = -1.0;
  zCoordinatesOfSphere =  0.0;

  contactPoints =
    delta::contactdetection::sphereToTriangle(
	  xCoordinatesOfSphere,
	  yCoordinatesOfSphere,
	  zCoordinatesOfSphere,
	  radius,
	  -1,
	  numberOfTriangles,
	  xCoordinates,
	  yCoordinates,
	  zCoordinates,
	  nullptr,
	  epsilon
	);

  if (contactPoints.size()!=1) return 1;
  if (contactPoints[0].x[0]!=0.0) return 1;
  if (contactPoints[0].x[1]!=-0.25) return 1;
  if (contactPoints[0].x[2]!=0.0) return 1;

  xCoordinatesOfSphere -= epsilon;
  yCoordinatesOfSphere  = 0;
  zCoordinatesOfSphere  = 0;

  contactPoints =
    delta::contactdetection::sphereToTriangle(
	  xCoordinatesOfSphere,
	  yCoordinatesOfSphere,
	  zCoordinatesOfSphere,
	  radius,
	  -1,
	  numberOfTriangles,
	  xCoordinates,
	  yCoordinates,
	  zCoordinates,
	  nullptr,
	  epsilon
	);

  if (contactPoints.size()!=1) return 1;
  if (contactPoints[0].x[0]!=(-epsilon + radius) / 2) return 1;
  if (contactPoints[0].x[1]!=0.0) return 1;
  if (contactPoints[0].x[2]!=0.0) return 1;

  return 0;
}
