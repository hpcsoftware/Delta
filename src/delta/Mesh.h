/*
 See license/copyright file at Delta's home www.peano-framework.org.

 Authors: Konstantinos Krestenitis, Tobias Weinzierl
 */
#ifndef _DELTA_MESH_H_
#define _DELTA_MESH_H_


#include "delta.h"


namespace delta {
  class Mesh;
}


/**
 * Interface for all meshes
 */
class delta::Mesh {
  public:
	/**
	 * Geometric centre of bounding sphere
	 */
	virtual iREAL getCentreX() const = 0;
	virtual iREAL getCentreY() const = 0;
	virtual iREAL getCentreZ() const = 0;

	virtual iREAL getBoundingSphereRadius() const = 0;


	/**
	 * Get the number of triangles held by this object.
	 */
	virtual int getNumberOfTriangles() const = 0;

	/**
	 * Returns a series of x coordinates. Every three entries give you the
	 * three coordinates of one triangle.
	 *
	 * The size of the returned array equals three times
	 * getNumberOfTriangles().
	 */
	virtual const iREAL* getXCoordinates() const = 0;
	virtual const iREAL* getYCoordinates() const = 0;
	virtual const iREAL* getZCoordinates() const = 0;
};


#endif

