/*
 See license/copyright file at Delta's home www.peano-framework.org.

 Authors: Konstantinos Krestenitis, Tobias Weinzierl
 */
#ifndef _DELTA_VFMESH_H_
#define _DELTA_VFMESH_H_


#include "delta.h"
#include "Mesh.h"


#include <vector>


namespace delta {
  class VFMesh;
}


/**
 * A VFMesh is a mesh which holds a set of vertices (V) and then faces (F) which
 * point to them. It comes along with a builder mechanism, i.e. you can add
 * vertices and faces step by step. Once you are done, you have to tell teh mesh
 * that you are done, so it can create the flattened representation we use
 * internally in Delta.
 *
 * @author Tobias Weinzierl
 */
class delta::VFMesh: public delta::Mesh {
  private:
	/**
	 * Min coordinates.
	 */
	iREAL _min[3];

	/**
	 * Max coordinates.
	 */
	iREAL _max[3];

	iREAL* _flattenedXCoordinates;
	iREAL* _flattenedYCoordinates;
	iREAL* _flattenedZCoordinates;

	struct Vertex {
	  iREAL x[3];
	  Vertex(
        const iREAL x,
        const iREAL y,
        const iREAL z
      );
	};

	struct Triangle {
	  int vertex[3];
	  Triangle( int a, int b, int c);
	};

	std::vector< Vertex >   _vertex;
	std::vector< Triangle > _triangle;

	void flatten();
	void freeFlattenedRepresentation();
  public:
	VFMesh();

	virtual ~VFMesh();

	iREAL getCentreX() const override;
	iREAL getCentreY() const override;
	iREAL getCentreZ() const override;

	iREAL getBoundingSphereRadius() const override;

	int getNumberOfTriangles() const override;
	const iREAL* getXCoordinates() const override;
	const iREAL* getYCoordinates() const override;
	const iREAL* getZCoordinates() const override;

	/**
	 * Create new vertex and obtain its index
	 */
	int addVertex(
	  const iREAL x,
	  const iREAL y,
	  const iREAL z
	);

	int addTriangle(
	  int index0,
	  int index1,
	  int index2
	);

	void finishedMeshBuildup();
};


#endif
