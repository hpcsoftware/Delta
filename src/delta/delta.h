#ifndef _DELTA_H_
#define _DELTA_H_


#include <string>


/**
 * @mainpage Delta source code page
 *
 * @image html ../logo.png
 *
 * Delta is a small toolbox/library to administer triangular meshes as they
 * stem from CAD geometries and to do basic geometric checks such as collision
 * checks. Delta's objective is to do these operations fast on modern hardware.
 * This site holds solely the source code documentation. For further
 * information (incl repository access), please visit the
 * <a href="http://www.peano-framework.org/index.php/delta/">HPC Software page</a>.
 *
 * The software package is mainly a library. The individual subdirectories,
 * which are mapped onto namespaces of their own, however also host some unit
 * tests, and I provide a set of simple integration tests that can also be seen
 * as examples how to get started. While the unit tests can be found in
 * subdirectories of their respective namespace, the these integration tests,
 * i.e. examples, are hosted in delta/tests.
 */

#ifndef iREAL
  #define iREAL double
#endif

#ifndef DELTA_DEBUG
  #define DELTA_DEBUG 1
#endif

namespace delta {
  std::string getOutputPrefix(const std::string& fullQualifiedMethodName);
}

#endif
