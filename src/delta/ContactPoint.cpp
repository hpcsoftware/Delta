#include "ContactPoint.h"
#include <iostream>
#include <iomanip>

delta::ContactPoint::ContactPoint() {}

delta::ContactPoint::ContactPoint(const ContactPoint& copy):
  distance(copy.distance),
  indexA(copy.indexA),
  indexB(copy.indexB) {

  x[0] = copy.x[0];
  x[1] = copy.x[1];
  x[2] = copy.x[2];

  normal[0] = copy.normal[0];
  normal[1] = copy.normal[1];
  normal[2] = copy.normal[2];

  indexA = copy.indexA;
  indexB = copy.indexB;
}


delta::ContactPoint::ContactPoint(
  const iREAL PA[3],
  int         indexA,
  const iREAL QB[3],
  int         indexB,
  const bool&     outside
):
  ContactPoint(
    PA[0], PA[1], PA[2], indexA,
    QB[0], QB[1], QB[2], indexB,
	outside
  ) {
}


#if defined(OMPProcess) || defined(OMPTriangle)
#pragma omp declare simd notinbranch
#endif
delta::ContactPoint::ContactPoint(
  const iREAL&  	xPA,
  const iREAL&  	yPA,
  const iREAL&  	zPA,
  int               indexA,

  const iREAL&  	xQB,
  const iREAL&  	yQB,
  const iREAL&  	zQB,
  int               indexB,

  const bool&       outside
):
  indexA(indexA),
  indexB(indexB) {
  x[0] = (xPA+xQB)/2.0;
  x[1] = (yPA+yQB)/2.0;
  x[2] = (zPA+zQB)/2.0;

  normal[0] = xPA-xQB;
  normal[1] = yPA-yQB;
  normal[2] = zPA-zQB;

  const iREAL normalLength = std::sqrt( normal[0]*normal[0] + normal[1]*normal[1] + normal[2]*normal[2] );

  normal[0] /= normalLength;
  normal[1] /= normalLength;
  normal[2] /= normalLength;

  distance = outside ? normalLength : -normalLength;

  #if DeltaDebug>=4
  std::cout << delta::getOutputPrefix("delta::ContactPoint::ContactPoint") << "create contact point between point "
            << std::setprecision(3)
            << "(" << xPA << "," << yPA << "," << zPA << ")^T"
			<< " and "
            << "(" << xQB << "," << yQB << "," << zQB << ")^T"
			<< ": " << toString()
		    << std::endl;
  #endif
}


std::string delta::ContactPoint::toString() const {
  std::ostringstream msg;
  msg << "(" << std::setprecision(3)
	  << "x=(" << x[0] << "," << x[1] << "," << x[2] << ")^T"
	  << ",n=(" << normal[0] << "," << normal[1] << "," << normal[2] << ")^T"
	  << ",d=" << distance
	  << ",index_A=" << indexA
	  << ",index_B=" << indexB
	  << ")";
  return msg.str();
}
