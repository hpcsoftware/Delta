#include "gts.h"
#include <fstream>
#include <iostream>
#include <sstream>


delta::Mesh* delta::io::loadGTSFile( const std::string& filename ) {
  delta::VFMesh* mesh = new delta::VFMesh();

  std::string line;

  std::ifstream ifs(filename);

  if (not ifs ) {
    std::cout << getOutputPrefix( "loadGTSFile(std::string)" ) <<  "can't open file " << filename << std::endl;
    return mesh;
  }

  std::getline(ifs, line);

  int numberOfVertices  = 0;
  int numberOfTriangles = 0;

  std::string token;

  try {
    #if DeltaDebug>1
    std::cout << getOutputPrefix( "loadGTSFile(std::string)" ) <<  "read line " << line << std::endl;
    #endif
    std::istringstream headerLine(line);
    headerLine >> token;

    numberOfVertices = std::stoi( token );

    headerLine >> token;
    if (token!="0") {
      std::cerr << getOutputPrefix( "loadGTSFile(std::string)" ) <<  "leg count should be zero" << std::endl;
    }

    headerLine >> token;
    numberOfTriangles = std::stoi( token );
  }
  catch (std::invalid_argument& exc) {
    std::cerr << getOutputPrefix( "loadGTSFile(std::string)" ) <<  "got exception from line " << line << ": " << exc.what() << std::endl;
    numberOfVertices  = 0;
    numberOfTriangles = 0;
  }


  std::cout << getOutputPrefix( "loadGTSFile(std::string)" ) <<  "read file " << filename << " containing " << numberOfVertices << " vertices and " << numberOfTriangles << " triangles" << std::endl;

  int lineNumber = 1;
  for (int i=0; i<numberOfVertices; i++) {
    std::getline(ifs, line);

    try {
      std::istringstream currentLine(line);
      currentLine >> token;
      const double x = std::stod( token );
      currentLine >> token;
      const double y = std::stod( token );
      currentLine >> token;
      const double z = std::stod( token );

      #if DeltaDebug>1
      std::cout << getOutputPrefix( "loadGTSFile(std::string)" ) <<  "add vertex " << x << " x " << y << " x " << z << " from line " << line << std::endl;
      #endif

      mesh->addVertex(x,y,z);
    }
    catch (std::invalid_argument& exc) {
      std::cerr << getOutputPrefix( "loadGTSFile(std::string)" ) <<  "got exception from in line " << lineNumber << ": " << exc.what() << std::endl;
      i = numberOfVertices;
    }

    lineNumber++;
  }

  #if DeltaDebug>1
  std::cout << getOutputPrefix( "loadGTSFile(std::string)" ) <<  "read all vertices till line " << lineNumber << std::endl;
  #endif

  for (int i=0; i<numberOfTriangles; i++) {
    std::getline(ifs, line);

    try {
      std::istringstream currentLine(line);
      currentLine >> token;
      const int x = std::stoi( token );
      currentLine >> token;
      const int y = std::stoi( token );
      currentLine >> token;
      const int z = std::stoi( token );

      #if DeltaDebug>1
      std::cout << getOutputPrefix( "loadGTSFile(std::string)" ) <<  "add triangle " << x << " x " << y << " x " << z << " from line " << line << std::endl;
      #endif

      mesh->addTriangle(x-1,y-1,z-1);
    }
    catch (std::invalid_argument& exc) {
      std::cerr << getOutputPrefix( "loadGTSFile(std::string)" ) <<  "got exception from in line " << lineNumber << ": " << exc.what() << std::endl;
      i = numberOfVertices;
    }

    lineNumber++;
  }

  ifs.close();

  mesh->finishedMeshBuildup();

  return mesh;
}
