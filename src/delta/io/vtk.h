/*
 See license/copyright file at Delta's home www.peano-framework.org.

 Authors: Tobias Weinzierl
 */
#ifndef _DELTA_IO_VTK_H_
#define _DELTA_IO_VTK_H_


#include "../delta.h"
#include "../ContactPoint.h"


#include <string>
#include <vector>


namespace delta {
  namespace io {
    /**
     * This is a plain dump routine which takes a flattened object
     * representation as described in the Mesh interface and dumps it into a
     * (legacy) vtk file.
     *
     * @param x Pointer to array with 3*numberOfTriangles entries
     * @param y Pointer to array with 3*numberOfTriangles entries
     * @param z Pointer to array with 3*numberOfTriangles entries
     * @param filename Ensure your filename ends with .vtk
     */
    void writeVTK(
      int    numberOfTriangles,
      const iREAL* x,
	  const iREAL* y,
	  const iREAL* z,
	  const std::string& filename
    );

    void writeVTK( const delta::ContactPoint& point, const std::string& filename );
    void writeVTK( const std::vector< delta::ContactPoint > & point, const std::string& filename );
  }
}


#endif
