/*
 See license/copyright file at Delta's home www.peano-framework.org.

 Authors: Tobias Weinzierl
 */
#ifndef _DELTA_IO_GTS_H_
#define _DELTA_IO_GTS_H_


#include "../delta.h"
#include <string>
#include <vector>
#include "../VFMesh.h"



namespace delta {
  namespace io {
    /**
     * This is a file format created by Duo Li which consists of columns that
     * describe the coordinates of the triangles.
     *
     * There are three indices in the first line of .gts file: number_of_vertices, number_of_legs (not used anymore), number_of_cells.
     * From hereon, the file format appendixes vertices as three tuples (x,y,z) and then the indices of the triangules (n1,n2,n3).
     * All enumeration is FORTRAN style, i.e. starting with 1.
     */
    Mesh* loadGTSFile( const std::string& filename );
  }
}



#endif
