\section{Introduction}


%
% Problem context
%
Handling triangulated meshes is an important task in many scientific
simulation codes.
To import a mesh---typically from a CAD model or a mesh repository---is one
of the first steps in many simulation pipelines, and the coding of appropriate
import routines, of mesh mappings onto existing compute grids, of routines that
identify where meshes touch is a fundamental technical task.
Economic common sense dictates to use a mature software component here.
In many traditional setups, the mesh import or generation, respectively, is a
sole preprocessing step.
Its efficiency thus has not highest priority. 


%
% Challenge
%
Our work however meanders around two use cases where computational efficiency of
the mesh handling is key:
Our first example from Discrete Element Methods (DEM).
Here, rigid particles as we find them in granulates or powder collide with each
other and exchange momentum.
We assume that the particles are represented by triangulated meshes
\cite{Krestenitis:18:DEMVectorisation,Krestenitis:18:DEMMultigrid}. 
In each and every time step, the meshes thus have to be compared to each other
and we have to identify collisions, i.e.~contact situations.
Non-monolithic fluid-structure interaction with moving meshes can
often been read as an generalisation of such a setup. 
One mesh wraps alike a cushion around the other.
It bumps against, i.e.~collides with the other mesh.
Our second example is dynamically adaptive mesh refinement (AMR) for mesh-based
solvers of partial differential equations that are combined with an immersed
boundary method \cite{Dumser:18:Immersed}.
As the simulation mesh or the embedded domain change, the simulation has to
identify the distance between newly created degrees of freedom and a boundary mesh
on-the-fly to impose the correct boundary terms.


%
% Requirements
%
Both demonstrator examples highlight on the functional side the need for mature,
easy to use meshing interfaces which allow users to code mesh input and
interaction functionality.
On the non-functional side, they clarify that it becomes important to deliver
geometric information effectively on modern supercomputer architectures.
Both demonstrators hereby rely on the same functionality: 
They handle at least two meshes and they have to know how far these meshes are
apart.
For an immersed boundary method, such information feeds directly into the
computation.
For a DEM method, such information identifies contact between structures once we
assume that any proximity closer than $\epsilon $ denotes contact.


%
% What we do: functionality
%
$\Delta $ proposes a simplistic programming interface (API) to load meshes, to
run primitive mesh modifications such as translations, scaling or rotations, 
and to compute distances as well as to detect contact and overlap between two
meshes.
The latter are passed an $\epsilon $ parameter, and they return for given
input data all contact points, i.e.~all points where the input data and the mesh
subject of study are closer than $\epsilon $.
Input data can be simple points (for the immersed boundary approach, e.g.) or
meshes themselves.
% Contact points are augmented with distance information.
Our implementation wraps around The Open-Asset Imports-Lib
\cite{Software:18:Assimp} and thus can load various different CAD formats.
We focus on triangular meshes here.
% , i.e.~the base library's triangulation
% facilities have to be used if meshes are not given via triangles.



%
% What we do: properties
%
Our code is optimised towards modern HPC architectures through various
techniques hidden behind the API:
(i) We flatten the triangle-edge-vertex graph describing the mesh into a plain
series of vertex triples as SoA.
% This is done on-demand.
The resulting data exhibits some memory overhead as vertices might be
replicated, but it can be streamed through the cores without indirect,
non-continuous memory access.
(ii) We introduce a hybrid collision detection algorithm which combines an
iterative Newton-based collision detection with traditional (if-based) distance
calculations.
The iterative approach vectorises. 
In cases where it does not converge, we fall back to the robust solve.
This results in a vectorised, robust algorithm.
(iii) The realisation behind the API runs parallel on multicore systems.
It parallelises over triangles.   
Furthermore, the API itself is thread-safe, i.e.~can be invoked from various
threads concurrently, and---for point-wise queries as they are used for immersed
boundary methods---offers routines that accept whole sets of query points over
which it parallelises, too.
(iv) Finally, $\Delta $ introduces spatial caching:
Upon demand or as preprocessing step, we wrap an octree data structure
around the mesh data.
If geometric proximity data in an $\epsilon $-environment is of interest,
distance queries are first compared against the octree.
This allows us to identify results further away than $\epsilon $ from any
geometry quickly.
As the octree can be built upon demand, its accuracy and overhead anticipate the
structure of the collision queries.
For a domain decomposition approach, the cache meta data structure automatically
follows the decomposition.
To the best of our knowledge, no other geometry library provides this unique
combination of efficient realisation ingredients.


The remainder is organised as follows: 
We first introduce our API (Section~\ref{section:api}) before we discuss details
of the used algorithms (Section~\ref{section:algorithms}).
Experimental data in Section~\ref{section:demonstrators} highlight the potential
of our $\Delta $ library.
We summarise our insight and close the discussion with Section~\ref{section:conclusion}.
An appendix (online version on software homepage only) offers how-to
information.
The software is freely available from \cite{Software:18:Delta}.

