\subsection{Vectorised triangle comparison}

$\Delta $'s contact point detection is solely based upon triangles.
It does, in its current version, neither exploit any mesh topology/hierarchy
nor identify higher-dimensional contact manifolds.
We thus assume, for the following discussion, that two sets of triangles
$\mathbb{T}_1$ and $\mathbb{T}_2$ are to be compared, and the algorithm compares all triangles from the former to the
latter.
Its complexity is $\mathcal{O}(|\mathbb{T}_1|\cdot |\mathbb{T}_2|)$.


\paragraph{Bounding sphere comparisons}
Our code supports a range of comparison routines.
The simplest type of comparison is the check of two spheres against each other. 
While this comparison is sufficient for solely sphere-based geometry models as
they are predominant in DEM, e.g., we emphasise another important role of the
sphere comparisons:
Comparisons of triangulated objects can be very costly.
It is thus reasonable to wrap them into a sphere pre-check.
If the bounding spheres of two objects do not overlap, we can skip all further
triangle comparisons.


\paragraph{Comparison-based }
The straightforward approach to compare two triangles is based upon some simple
geometric considerations:
The closest point between two triangles is either defined by a point from one
triangle and a face of the other triangle, or it is found between the edges of
two triangles.
Once all distances between these
geometry combinations are known, we can construct the closest distance by a
simple minimum selection.
If this distance is smaller than $2 \epsilon $, two triangles yield a contact
point by our definition.

\begin{figure}[htb]
 \begin{center}
  \includegraphics[width=0.4\textwidth]{illustrations/geometric.pdf}
 \end{center}
 \caption{
  Geometric checks for a triangulated volume approaching a plane (at the bottom)
  spanned by four triangles A,B,C,D.
  The comparison of triangle X to A finds the contact point (shortest line)
  through a projection of a vertex of X onto A. 
  Comparing X to C first obtains the same point projecting the vertex onto the
  plane spanned by C, but then it has to move the anchor point on the plane into
  C.
  When we compare X to B, the closest line is found between two edges.
  \label{figure:algorithms:geometric}
 }
\end{figure}


The implementation of this algorithm in $\Delta $ is a simple sequence of
geometric comparisons.
We compare each point of the two triangles against the other triangle.
This gives us $2 \cdot 3$ combinations.
For the comparison of the three edges of the two triangles, we obtain $3 \cdot
2 \cdot 1$ combinations to check. We exploit symmetries.
The number of geometric setups that are to be compared totals to 15
(Figure~\ref{figure:algorithms:geometric}).


While a constant complexity/variant count per se is nothing bad, we note that
the individual comparisons require additional checks. 
Whenever we project a vertex from one triangle onto the plane spanned by the
triangle counterpart, we next have to check whether an orthogonal
projection ends up within the triangle.
Otherwise, the closest point-to-triangle point hits an edge.
Analogous considerations holds for edges.



The code is heavy on if statements.
We study its vectorisation behaviour in \cite{Krestenitis:18:DEMVectorisation}
and demonstrate that it struggles to vectorise and to stream data through the
core.
However, this code variant always
yields the exact result.
It is robust.



\paragraph{An iterative approach}

An alternative approach casts both comparison triangles into their barycentric
representation defined by four scalars $a_1,a_2,a_3,a_4 \in [0,1]$.
Let the triangles be written as points $T_1(a_1,a_2), T_2(a_3,a_4) \in
\mathbf{R}^3$ in the three-dimensional space.
The closest point between the two triangles is given as 
\[
\min_{a_1,a_2,a_3,a_4} \frac{1}{2} |T_1(a_1,a_2)-T_2(a_3,a_4)|^2_2 \quad
\mbox{subject to}\ a,b,c,d \in [0,1].
\]

\noindent
We cast this minimisation problem with a Lagrangian $\alpha _L>0$ multiplier
into a weak formulation
\begin{equation}
  \min_{a_1,a_2,a_3,a_4} \frac{1}{2} |T_1(a_1,a_2)-T_2(a_3,a_4))|^2_2 
  + \alpha _L \sum _i \max(a_i-1,0) + \max(-a_i,0)
  \label{equation:algorithms:iterative}
\end{equation}

\noindent
and solve it via a Newton method. 
The system can become notoriously ill-conditioned for close-to-parallel
triangles and thus requires some regularisation of the underlying Jacobian. 
We add a diagonal matrix scaled with an additional regularisation 
weight $ \alpha _R$.
All matrix inverts from (\ref{equation:algorithms:iterative}) then can be
written down explicitly as we are solely working in the three-dimensional space.
With $\max $ using simple masking within the chip, the code almost lacks ifs.


We obtain an iterative algorithm with high arithmetic
intensity---notably if we run the Newton for multiple triangles in one rush.
Its problem is that it is not robust even though the majority of
triangle-to-triangle comparisons converges after less than four iterations.


\paragraph{A hybrid approach}
It is easy for  (\ref{equation:algorithms:iterative}) to track the residual per
step.
The resulting Newton update size is a reasonable indicator for convergence if
Lagrangian and regularisation parameters $\alpha _L, \alpha _R$ are well-chosen.
If this error estimator is big after a fixed number of steps, we may
assume that the iterative scheme has not yet converged and is potentially
ill-conditioned.


Empirical studies show that the iterative scheme is by more than a factor of 10
faster then the geometric checks, if we constrain the number of Newton steps to
4--8.
$\Delta $'s hybrid approach thus runs a prescribed number of Newton steps.
Hardcoding the iterations has the nice side effect that we can unroll these
steps.
After the fixed number of iterations, it checks the termination criterion. 
If it exceeds a prescribed error threshold, the code falls back to the pure
geometric checks.


We end up with a geometric comparison duet which is empirically by a factor of
two slower than a purely iterative approach, but, different to this one, is
absolutely stable; 
and still more than a factor of 5 faster than an if-based only 
approach \cite{Krestenitis:18:DEMVectorisation,Krestenitis:18:DEMMultigrid}.
The iterative approach is computationally more demanding than its geometric
counterpart.
The gain in speed is thus solely due to vectorisation which renders this
approach promising for state-of-the-art architectures.
